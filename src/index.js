import 'bootstrap/dist/css/bootstrap-grid.min.css';
import $ from 'jquery';
import './scss/style.scss';

global.jQuery = $;
global.$ = $;

$(document).ready(function($) {
    const TAX_FEE = 10;

    let cart = [
        { "rowId" : 1,  "qty" : 1, "price" : 100, "subTotal" : 100},
        { "rowId" : 2,  "qty" : 1, "price" : 100, "subTotal" : 100},
        { "rowId" : 3,  "qty" : 1, "price" : 100, "subTotal" : 100}
    ];

    function deleteCartRow(rowId) {
        cart = cart.filter(row => row.rowId !== rowId);
    }

    function uprateCartRow(rowId, newQty) {
        let row = cart.find(row => row.rowId === rowId);
        if (row) {
            row.qty = newQty;
            row.subTotal = row.qty * row.price;
        }
    }

    function getCartRowSubtotal(rowId) {
        let subTotal = 0;
        let row = cart.find(row => row.rowId === rowId);
        if (row) {
            subTotal = row.subTotal ;
        }

        return subTotal;
    }

    function calculateTotal() {
        let total = 0;
            total = cart.reduce(
                (accumulator, currentValue) => accumulator + currentValue.subTotal,
                total
            );

        return total;
    }

    function updateSummary() {
        let total = calculateTotal();
        $('#cart-subtotal').text(`: $${total}`);
        $('#cart-total').text(`: $${total + TAX_FEE}`);
    }

    $('.btn-close').on("click", function(event){
        let parent = $(event.target).parents(".Rtable-row")[0];
        let rowId = $(parent).data('rowId');
        $(parent).html('').css('border','none');
        deleteCartRow(rowId);
        updateSummary();
    });

    $('.qty-cell input').on("change", function(event){
        let parent = $(event.target).parents(".Rtable-row")[0];
        let rowId = $(parent).data('rowId');
        let newQty = $(event.target)[0].value;
        uprateCartRow(rowId, newQty);
        let subTotal = getCartRowSubtotal(rowId);
        $('#subtotal-row' + rowId).text(`$ ${subTotal}`);
        updateSummary();
    });
});