let webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin =require('copy-webpack-plugin');
const autoprefixer = require('autoprefixer');


let conf = {
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname,'./dist'),
		filename: 'main.js',
		publicPath: 'dist/'
	},
	devServer: {
		overlay: true
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
			},
			{
		        test: /\.scss$/,
		        use: ExtractTextPlugin.extract(
		          {
		            use: ['css-loader',
		            	{
			              loader: 'postcss-loader',
			              options: {
			                plugins: () => [autoprefixer()]
			              }
			            },
			            'sass-loader'],
		          })
		    },
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					use: ['css-loader']
				})
			},
			{
				test: /\.(png|jpg|gif|eot|otf|ttf|woff|woff2)$/i,
				use: [
					{
						loader: 'url-loader'
					},
				],
			},
		]
	},
	plugins: [
		new ExtractTextPlugin(
	      {filename: 'style.css'}
	    ),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery'
		}),
		new CopyWebpackPlugin([{
				from: './src/fonts',
				to: './fonts'
			},
			{
				from: './src/images',
				to: './images'
			}
		]),
	]
};

module.exports = conf;